# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: Proto/BucketMessage.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='Proto/BucketMessage.proto',
  package='Bojd.Messages.MessageContent',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x19Proto/BucketMessage.proto\x12\x1c\x42ojd.Messages.MessageContent\"\x81\x01\n\rBucketMessage\x12\x0e\n\x06UserId\x18\x01 \x01(\x05\x12\x0c\n\x04Size\x18\x02 \x01(\x05\x12\x0c\n\x04Name\x18\x03 \x01(\t\x12\x0e\n\x06Public\x18\x04 \x01(\x08\x12\x10\n\x08\x42ucketId\x18\x05 \x01(\x05\x12\x0f\n\x07GroupId\x18\x06 \x01(\x05\x12\x11\n\tSpaceLeft\x18\x07 \x01(\x02\x62\x06proto3')
)




_BUCKETMESSAGE = _descriptor.Descriptor(
  name='BucketMessage',
  full_name='Bojd.Messages.MessageContent.BucketMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UserId', full_name='Bojd.Messages.MessageContent.BucketMessage.UserId', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Size', full_name='Bojd.Messages.MessageContent.BucketMessage.Size', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Name', full_name='Bojd.Messages.MessageContent.BucketMessage.Name', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Public', full_name='Bojd.Messages.MessageContent.BucketMessage.Public', index=3,
      number=4, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BucketId', full_name='Bojd.Messages.MessageContent.BucketMessage.BucketId', index=4,
      number=5, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='GroupId', full_name='Bojd.Messages.MessageContent.BucketMessage.GroupId', index=5,
      number=6, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SpaceLeft', full_name='Bojd.Messages.MessageContent.BucketMessage.SpaceLeft', index=6,
      number=7, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=60,
  serialized_end=189,
)

DESCRIPTOR.message_types_by_name['BucketMessage'] = _BUCKETMESSAGE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

BucketMessage = _reflection.GeneratedProtocolMessageType('BucketMessage', (_message.Message,), dict(
  DESCRIPTOR = _BUCKETMESSAGE,
  __module__ = 'Proto.BucketMessage_pb2'
  # @@protoc_insertion_point(class_scope:Bojd.Messages.MessageContent.BucketMessage)
  ))
_sym_db.RegisterMessage(BucketMessage)


# @@protoc_insertion_point(module_scope)
