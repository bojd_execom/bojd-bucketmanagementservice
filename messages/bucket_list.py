from communication.message_publish import MessagePublish


class BucketList(MessagePublish):
    def __init__(self, request_id, protobuf_message=''):
        super().__init__(message_object='AllBuckets')
        self.content = protobuf_message
        self.request_id = request_id
