from communication.message_publish import MessagePublish


class FileReturn(MessagePublish):
    def __init__(self, request_id, protobuf_message=''):
        super().__init__(message_object='File')
        self.content = protobuf_message
        self.request_id = request_id
