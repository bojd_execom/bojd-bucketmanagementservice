from communication.message_publish import MessagePublish


class ErrorDeleteBucket(MessagePublish):
    def __init__(self, request_id, protobuf_message=''):
        super().__init__(message_object='ErrorDeleteBucket')
        self.request_id = request_id
