from communication.message_publish import MessagePublish


class ErrorGetFile(MessagePublish):
    def __init__(self, request_id, protobuf_message=''):
        super().__init__(message_object='ErrorGetFile')
        self.request_id = request_id
