from communication.message_publish import MessagePublish
  
  
class ErrorUploadFile(MessagePublish):
    def __init__(self, request_id, protobuf_message=''):
        super().__init__(message_object='ErrorFileUpload')
        self.request_id = request_id
