from communication.message_publish import MessagePublish


class ErrorCreateBucket(MessagePublish):
    def __init__(self, request_id, protobuf_message=''):
        super().__init__(message_object='ErrorCreateBucket')
        self.request_id = request_id
