ALLOWED_BUCKET_SIZES = [1, 2, 5]


def validate_bucket_info(size, name, owner_id):
    return size in ALLOWED_BUCKET_SIZES and owner_id != -1 and name
