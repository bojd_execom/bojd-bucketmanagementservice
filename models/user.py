from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from database.database import Base


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    user_uid = Column(Integer, unique=True)
    buckets = relationship("Bucket", back_populates="owner", lazy="subquery")

    def __init__(self, user_uid=None):
        self.user_uid = user_uid

    def __repr__(self):
        return '<User %r>' % self.user_uid
