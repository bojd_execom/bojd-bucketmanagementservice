from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from database.database import Base


class Bucket(Base):
    __tablename__ = 'buckets'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    size = Column(Integer)
    public = Column(Boolean)
    name_on_disk = Column(String(100))
    owner_id = Column(Integer, ForeignKey('users.id'))
    owner = relationship("User", back_populates="buckets", lazy="subquery")
    space_left = 0    

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<User %r>' % self.name
