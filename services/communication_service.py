import threading
from communication.service_base import ServiceBase
from errors.error_convertor import convert_service_error_to_message
from messages.bucket_created import BucketCreated
from messages.bucket_deleted import BucketDeleted
from messages.bucket_edited import BucketEdited
from messages.bucket_list import BucketList
from messages.error_messages.error_delete_bucket import ErrorDeleteBucket
from messages.error_messages.error_edit_bucket import ErrorEditBucket
from messages.error_messages.error_get_file import ErrorGetFile
from messages.file_list import FileList
from messages.bucket_return import BucketReturn
from messages.error import Error
from messages.file_return import FileReturn
from messages.file_uploaded import FileUploaded
from messages.message_content.BucketMessage_pb2 import BucketMessage
from messages.message_content.FileMessage_pb2 import FileMessage
from messages.message_content.UserMessage_pb2 import UserMessage
from messages.message_content.BucketsInfoMessage_pb2 import BucketsInfoMessage
from messages.message_content.FilesInfoMessage_pb2 import FilesInfoMessage
from services.bucket_service import BucketService


class CommunicationService(ServiceBase):
    def __init__(self):
        super().__init__()
        self.__bucket_service = BucketService()
        self.subscribe(b'Publish.CreateBucket', self._handle_create_bucket)
        self.subscribe(b'Publish.UploadFile', self._handle_add_file)
        self.subscribe(b'Get.Files', self._handle_get_bucket_files)
        self.subscribe(b'Get.AllBuckets', self._handle_get_all_buckets)
        self.subscribe(b'Get.Bucket', self._handle_get_bucket)
        self.subscribe(b'Get.File', self._handle_get_bucket_file)
        self.subscribe(b'Publish.EditBucket', self._handle_edit_bucket)
        self.subscribe(b'Publish.DeleteBucket', self._handle_delete_bucket)

    def start(self):
        while True:
            message = self._subscriber.recv_multipart()
            print('received message')
            topic = message[0]
            working_thread = threading.Thread(target=self._subscriptions[topic], args=(message,))
            working_thread.start()
            print('responding..')

    def _handle_create_bucket(self, message):
        request_message = BucketMessage()
        request_message.ParseFromString(message[2])  # parsing content
        result, error = self.__bucket_service.create_bucket(
            request_message.Size, 
            request_message.UserId, 
            request_message.Name,  
            request_message.Public)
        message_id = message[1]
        if error:
            error_message = convert_service_error_to_message(error)
            result_message = Error(request_id=message_id, protobuf_message=error_message)
        else:
            result_message = BucketCreated(request_id=message_id)
        self.send(result_message.to_protobuf_message())

    def _handle_add_file(self, message):
        request_message = FileMessage()
        request_message.ParseFromString(message[2])  # parsing content
        message_id = message[1]
        result, error = self.__bucket_service.add_file(
            request_message.BucketId,
            request_message.FileName,
            request_message.FileContent,
            request_message.Extension)
        if not error:
            result_message = FileUploaded(request_id=message_id)
        else:
            error_message = convert_service_error_to_message(error)
            result_message = Error(request_id=message_id, protobuf_message=error_message)
        self.send(result_message.to_protobuf_message())

    def _handle_get_all_buckets(self, message):
        request_message = UserMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]
        buckets, error = self.__bucket_service.get_all_buckets_for_user(request_message.Id)
        if not error:
            buckets_protobuf = BucketsInfoMessage()
            bucket_messages = []
            for bucket in buckets:
                bucket_message = BucketMessage()
                bucket_message.Name = bucket.name
                bucket_message.BucketId = bucket.id
                bucket_message.Size = bucket.size
                bucket_message.Public = bucket.public
                bucket_message.SpaceLeft = bucket.space_left
                bucket_messages.append(bucket_message)
            buckets_protobuf.Buckets.extend(bucket_messages)
            result_message = BucketList(request_id=message_id, protobuf_message=buckets_protobuf)
        else:
            error_message = convert_service_error_to_message(error)
            result_message = Error(request_id=message_id, protobuf_message=error_message)
        self.send(result_message.to_protobuf_message())

    def _handle_get_bucket_files(self, message):
        request_message = BucketMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]
        files_dict, error = self.__bucket_service.get_bucket_files(request_message.BucketId)
        print(files_dict, error)
        if not error:
            files_protobuf = FilesInfoMessage()
            files_messages = [] 
            for file, size in files_dict.items():
                file_message = FileMessage()
                file_message.FileName = file
                file_message.Size = size
                file_message.BucketId = request_message.BucketId
                files_messages.append(file_message)
            files_protobuf.Files.extend(files_messages)
            result_message = FileList(request_id=message_id, protobuf_message=files_protobuf)
        else:
            error_message = convert_service_error_to_message(error)
            result_message = Error(request_id=message_id, protobuf_message=error_message)
        self.send(result_message.to_protobuf_message())

    def _handle_get_bucket(self, message):
        request_message = BucketMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]
        bucket, error = self.__bucket_service.get_bucket(request_message.BucketId)
        if not error:
            bucket_message = BucketMessage()
            bucket_message.Name = bucket.name
            bucket_message.BucketId = bucket.id
            bucket_message.Size = bucket.size
            bucket_message.Public = bucket.public
            bucket_message.SpaceLeft = bucket.space_left
            result_message = BucketReturn(request_id=message_id, protobuf_message=bucket_message)
        else:
            error_message = convert_service_error_to_message(error)
            result_message = Error(request_id=message_id, protobuf_message=error_message)
        self.send(result_message.to_protobuf_message())
        
    def _handle_edit_bucket(self, message):
        request_message = BucketMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]
        result, error = self.__bucket_service.edit_bucket(
            request_message.BucketId,
            request_message.Size,
            request_message.UserId,
            request_message.Name,
            request_message.Public
        )
        print('dosao dovde')
        if not error:
            result_message = BucketEdited(request_id=message_id)
        else:
            error_message = convert_service_error_to_message(error)
            result_message = Error(request_id=message_id, protobuf_message=error_message)
        self.send(result_message.to_protobuf_message())

    def _handle_delete_bucket(self, message):
        request_message = BucketMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]
        result, error = self.__bucket_service.delete_bucket(request_message.BucketId, request_message.UserId)
        if not error:
            result_message = BucketDeleted(request_id=message_id)
        else:
            error_message = convert_service_error_to_message(error)
            result_message = Error(request_id=message_id, protobuf_message=error_message)
        self.send(result_message.to_protobuf_message())

    def _handle_get_bucket_file(self, message):
        request_message = FileMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]
        file, error = self.__bucket_service.get_file(
            request_message.BucketId,
            request_message.FileName
        )
        if not error:
            file_message = FileMessage()
            file_message.FileName = request_message.FileName
            file_message.BucketId = request_message.BucketId
            file_message.FileContent = file
            result_message = FileReturn(request_id=message_id, protobuf_message=file_message)
        else:
            error_message = convert_service_error_to_message(error)
            result_message = Error(request_id=message_id, protobuf_message=error_message)
        self.send(result_message.to_protobuf_message())
