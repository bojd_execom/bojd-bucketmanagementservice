import os
import shutil
import uuid

from errors.service_error import ServiceError
from models.bucket import Bucket
from models.user import User
from database.database import get, get_or_create, save, delete
from utils.files_util import get_directory_size, convert_bytes_to_gb
from validation.bucket_validation import validate_bucket_info


class BucketService:
    def __init__(self):
        self.path = r'/wd/'
        # self.path = r'C:\Users\jdragutinovic\Desktop\buckets\\'

    def create_bucket(self, size, owner_id, name, public):
        print('size', size)
        print('owner_id', owner_id)
        print('name', name)
        if not validate_bucket_info(size, name, owner_id):
            return None, ServiceError(400, 'You did not pass valid data.')
        bucket_disk_name = str(uuid.uuid4())
        new_path = os.path.join(self.path, bucket_disk_name)
        try:
            owner, created = get_or_create(User, user_uid=owner_id)
            if not created:
                bucket_with_same_name = any(bucket.name == name for bucket in owner.buckets)
                if bucket_with_same_name:  # check if user has bucket with that name already
                    return None, ServiceError(409, 'Bucket with same name already exists.')
            print('Creating bucket...')
            os.makedirs(new_path)
            new_bucket = Bucket()
            new_bucket.name = name
            new_bucket.name_on_disk = bucket_disk_name
            new_bucket.size = size
            new_bucket.public = public
            new_bucket.owner = owner
            save(new_bucket)
            print('Created bucket!')
            return True, None
        except OSError:
            return None, ServiceError(500, 'Error while creating bucket on disk.')
    
    def get_bucket(self, bucket_id):
        print('id', bucket_id)
        bucket = get(Bucket, id=bucket_id)
        if bucket:
            return bucket, None
        return None, ServiceError(400, 'No bucket with given id.')

    def get_all_buckets_for_user(self, user_id):
        owner = get(User, user_uid=user_id)
        if not owner:  # user didn't create any buckets yet
            return [], None
        buckets = owner.buckets
        for bucket in buckets:
            bucket.space_left = bucket.size - convert_bytes_to_gb(
                get_directory_size(os.path.join(self.path, bucket.name_on_disk))
            )
        return buckets, None

    def delete_bucket(self, bucket_id, user_id):
        bucket = get(Bucket, id=bucket_id)
        if bucket.owner.user_uid != user_id:
            print('User doesnt have privilegies to delete bucket.')
            return None, ServiceError(403, 'User doesnt have privilegies to delete bucket.')
        
        bucket_path = os.path.join(self.path, bucket.name_on_disk)
        if os.path.exists(bucket_path):
            print('removing bucket.. from: ' + bucket_path)
            shutil.rmtree(bucket_path)  # removes entire directory (with all files inside)
            delete(bucket)
            return True, None
        print('No bucket on disk.')
        return None, ServiceError(500, 'Error while deleting bucket on disk.')

    def edit_bucket(self, bucket_id, size, owner_id, name, public):
        bucket = get(Bucket, id=bucket_id)
        if not bucket:
            print('Wrong bucket id')
            return None, ServiceError(400, 'Wrong bucket id.')
        if bucket.owner.user_uid != owner_id:
            print('You dont have permissions to edit this bucket.')
            return None, ServiceError(403, 'You do not have permissions to edit this bucket.')
        # check size before changing it in database (if going from bigger to smaller size)
        # check how will group access change
        if not validate_bucket_info(size, name, owner_id):
            print('Invalid data.')
            return None, ServiceError(400, 'Invalid data.')
        owner = get(User, user_uid=bucket.owner.user_uid)  # getting user from database to get his buckets
        bucket_with_same_name = any(bucket.name == name and bucket.id != bucket_id for bucket in owner.buckets)
        if bucket_with_same_name:  # check if user has bucket with that name already
            print('Bucket with same name exists.')
            return None, ServiceError(409, 'Bucket with the same name exists.')
        print('Editing bucket.')
        bucket.name = name
        bucket.public = public
        bucket.size = size
        save(bucket)
        return True, None

    def add_file(self, bucket_id, name, content, extension):
        bucket = get(Bucket, id=bucket_id)
        if not bucket:
            print('Wrong bucket id')
            return None, ServiceError('Wrong bucket id.')
        # check if user has full bucket first
        # check if file is over 100 mb
        file_path = os.path.join(self.path, bucket.name_on_disk, name + '.' + extension)
        if not os.path.exists(file_path):
            print("uploading file.. to: " + file_path)
            new_file = open(file_path, "wb")
            new_file.write(content)
            return True, None
        print('File with same name exists.')
        return False, ServiceError(409, 'File with the same name exists.')

    def get_file(self, bucket_id, file_name):
        bucket = get(Bucket, id=bucket_id)
        if not bucket:
            print('Wrong bucket id.')
            return None, ServiceError(400, 'Wrong bucket id.')
        print('Getting file..')
        path_to_file = os.path.join(self.path, bucket.name_on_disk, file_name)
        file = open(path_to_file, "rb")  # opening for [r]eading as [b]inary
        binary_data = file.read()
        file.close()
        return binary_data, None

    def remove_file(self, bucket_id, name):
        new_path = self.path + str(bucket_id) + str(name)
        print('removing file.. from: ' + new_path)
        if os.path.exists(new_path):
            os.remove(new_path)  # removes only files

    def get_bucket_files(self, bucket_id):
        bucket = get(Bucket, id=bucket_id)
        bucket_path = os.path.join(self.path, bucket.name_on_disk)
        if os.path.exists(bucket_path):
            print("getting all files.. from: " + bucket_path)
            files_and_size = dict()  # dictionary that contains file-size, key-value

            for (path, dirs, files) in os.walk(bucket_path):  # goes trough directory and gets files and their size
                for file in files:
                    file_path = os.path.join(path, file)
                    file_size = os.path.getsize(file_path)
                    files_and_size[file] = file_size
            return files_and_size, None
        return None, ServiceError(500, 'Bucket does not exist on disk.')
