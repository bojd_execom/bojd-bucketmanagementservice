from abc import ABC, abstractmethod
import zmq


class ServiceBase(ABC):
    def __init__(self):
        context = zmq.Context()
        self._dealer = context.socket(zmq.DEALER)
        self._subscriber = context.socket(zmq.SUB)
        self._dealer.connect('tcp://localhost:9000')
        self._subscriber.connect('tcp://localhost:9001')
        self._subscriptions = dict()

    @abstractmethod
    def start(self):
        pass

    def send(self, message):
        self._dealer.send_multipart(message)

    def subscribe(self, topic, action):
        self._subscriber.setsockopt(zmq.SUBSCRIBE, topic)
        self._subscriptions[topic] = action
