from messages.message_content.ErrorMessage_pb2 import ErrorMessage


def convert_service_error_to_message(service_error):
    error_message = ErrorMessage()
    error_message.Code = service_error.code
    error_message.Text = service_error.text
    return error_message
