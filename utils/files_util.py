import os


def get_directory_size(path):
    return sum(os.path.getsize(os.path.join(path, f)) for f in os.listdir(path))


def convert_bytes_to_gb(byte_size):
    return byte_size / 1024 / 1024 / 1024
