from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# mysql_engine = create_engine('mysql+mysqlconnector://root:@localhost')

# mysql_engine.execute('CREATE DATABASE IF NOT EXISTS bucket_service')

# engine = create_engine('mysql+mysqlconnector://root:@localhost/bucket_service', convert_unicode=True)
engine = create_engine('sqlite:///database.sqlite', convert_unicode=True)
Session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))


Base = declarative_base()
Base.query = Session.query_property()


def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import models
    Base.metadata.create_all(bind=engine)


def get_or_create(model, **kwargs):
    """
    Gets model from database or creates it if it doesnt't already exists.
    :param model: Model type
    :param kwargs: Filtering criteria
    :return: Found or created instance and flag which indicates if instance was created or returned from database
    """
    session = Session() 
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        session.close()
        return instance, True


def get(model, **kwargs):
    session = Session()
    instance = session.query(model).filter_by(**kwargs).first()
    session.close()
    return instance


def save(model):
    session = Session()
    session.add(model)
    session.commit()
    session.close()


def delete(model):
    session = Session()
    session.delete(model)
    session.commit()
    session.close()
